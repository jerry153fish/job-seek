# ########################## ###################### ############################
#                                                                              #
# Author: Jerry Chen <jerry153fish@gmail.com>                                  #
# Date: July 2017                                                              #
# License: MIT                                                                 #
#                                                                              #
# Jobseeker is a simple job seek and resume generator in python                #
# https://gitlab.com/jerry153fish/job-seek                                     #
#                                                                              #
# There are three main components in jobseeker: scrapy, notices and matcher    #
# Scrapy: Scrapy the main Employment websites                                  #
#                                                                              #
# Notices: generate resume and notice user                                     #
#                                                                              #
# matcher: match the job to user profile                                       #
#                                                                              #
# ##############################################################################

from scrapies.seek.seek_com_au import SeekScrapy
from model.model import Job
from controller.jobcontroller import JobController
from controller.emailcontroller import send_email
from controller.pdfcontroller import PdfController
from controller.docxcontroller import DocxController
import sys
import yaml

if __name__ == '__main__':
    # print(sys.argv)
    test = SeekScrapy()
    pdf = PdfController()
    dc = DocxController()
    with open("config.yml", 'r') as fd:
        profile = yaml.load(fd)
        # print(profile['projects'])
        # pdf.generate_pdf(profile, 'out.pdf')
        # dc.generate_docx(profile,'out.docx')
    test.find('programmer', 'Hobart')
    # send_email('jerry153fish@gmail.com', 'hebeizhong9528', 'jerry.chen@wherewot.com', 'aa', 'hello','out.pdf')
