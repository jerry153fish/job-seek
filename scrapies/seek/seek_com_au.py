from scrapies.scrapy_base import ScrapyBase
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import json
from time import sleep
from threading import Thread
import queue
from bs4 import BeautifulSoup as bs
import re
from lxml import html
from random import randint
from controller.jobcontroller import JobController

# seek.com.au scrpay class
class SeekScrapy(ScrapyBase):
    def __init__(self):
        # set start url
        super(SeekScrapy, self).__init__('https://www.seek.com.au/')
        self.simple_jobs = queue.Queue()
        self.detailed_jobs = queue.Queue()
        self.urls = queue.Queue()
        self.jobs = []

    # find jobs in seek.com.au
    # parameters: keys: keywords, loc: location
    # return list job dict generater
    def find(self, keys, loc):
        browser = self.selenium_get(self.url)
        # simulate input keys and location and press enter key
        self._prepare_input(browser, "SearchBar__Keywords", keys)

        self._prepare_input(browser, "SearchBar__Where", loc).send_keys(Keys.RETURN)

        # get all page urls
        urls, total_num = self._generate_urls(browser)
        print(urls)

        for u in urls:
            self.urls.put(u)
        browser.close()

        num = 4 if len(urls)> 4 else len(urls)
        dnum = 4 if total_num > 4 else total_num
        # 4 thread to get simple jobs
        for i in range(num):
            t = Thread(target=self._get_jobs_handler)
            t.setDaemon(True)
            t.start()

        for i in range(dnum):
            t = Thread(target=self._get_detail_job_handler)
            t.setDaemon(True)
            t.start()

        t1 = Thread(target=self._save_detail_job_handler)
        t1.setDaemon(True)
        t1.start()

        self.urls.join()
        self.simple_jobs.join()
        self.detailed_jobs.join()


        # print(list(self.detailed_jobs.queue))

    # based on total number of jobs generate page enter
    def _generate_urls(self, browser):
        # wait for get total number of jobs
        try:
            total_num = WebDriverWait(browser, 20).until(
                EC.presence_of_element_located((By.XPATH, '//*[@id="SearchSummary"]/strong'))
            )
        except Exception as e:
            raise
        # get current_url
        url = browser.current_url
        total_num = int(total_num.text.replace(',',''))
        # get page numbers each page of 20 jobs
        pages = int(total_num / 20)
        if pages > 1:
            return [url] + [url + '?page=' + str(i) for i in range(2, pages+2)], total_num
        else:
            return [url], total_num

    # prepare for input
    def _prepare_input(self, browser, id, keys):
        try:
            elem = WebDriverWait(browser, 20).until(
                EC.presence_of_element_located((By.ID, id))
            )
        except Exception as e:
            raise
        elem.clear()
        elem.send_keys(keys)
        return elem

    def _get_jobs_handler(self):
        while True:
            url = self.urls.get()
            self._get_jobs(url)
            self.urls.task_done()

    def _get_detail_job_handler(self):
        while True:
            job = self.simple_jobs.get()
            self._get_detail_job(job)
            self.simple_jobs.task_done()

    def _save_detail_job_handler(self):
        while True:
            job = self.detailed_jobs.get()
            jobc = JobController(job)
            jobc.save()
            self.detailed_jobs.task_done()

    # get jobs of page
    def _get_jobs(self, url):
        try:
            self._get_simple_jobs_by_request(url)

        except Exception as e:
            print(e)
            try:
                self._get_simple_jobs_by_selenium(url)
            except Exception as e:
                pass


    def _get_simple_jobs_by_request(self, url):
        sleep(randint(10,100)/100)
        response = self.request_get(url)
        if response.status_code != 200:
            print('url error')
            raise Exception('Url err')
        soup = html.fromstring(response.content)
        articles = soup.xpath('//article')
        if articles and len(articles) == 0:
            print('not found')
            raise Exception('Not find')

        for a in articles:
            job = json.loads(a.xpath('.//script/text()')[0])
            job['jtype'] = {
                'name': a.xpath(".//a[@data-automation='jobSubClassification']/text()")[0],
                'industry': job.pop('industry')
            }
            self.simple_jobs.put(self._format_job_data(job))

    def _format_job_data(self, job):
        job['organization'] = job.pop('hiringOrganization')
        loc = job.pop('jobLocation')
        job['location'] = loc['address']['addressRegion']
        job['name'] = job.pop('title')
        return job

    def _get_simple_jobs_by_selenium(self, url):
        browser = self.selenium_get(url)
        try:
            articles = WebDriverWait(browser, 20).until(
                EC.presence_of_all_elements_located((By.TAG_NAME, "article"))
            )
            for a in articles:
                json_str = a.find_element_by_xpath('.//script').get_attribute("innerHTML")
                job = json.loads(json_str)
                prof = a.find_element_by_xpath(".//a[@data-automation='jobSubClassification']").get_attribute("innerHTML")
                job['jtype'] = {
                    'name': prof,
                    'industry': job.pop('industry')
                }
                self.simple_jobs.put(self._format_job_data(job))
            browser.close()
        except Exception as e:
            print(e)
            raise

    def _get_detail_job_by_request(self, job):
        sleep(randint(10,100)/100)
        response = self.request_get(job['url'])
        if response.status_code != 200:
            raise Exception('Url err')
        soup = html.fromstring(response.content)
        details = soup.xpath('//div[contains(@class, "job-template__wrapper")]')
        if len(details) > 0:
            details = details[0]
            side = soup.xpath('//aside')[0]
            job['salary'] = self.extract_salary_from_string(side.text_content())
            all_text = details.text_content()
            job['email'] = self.extract_email_from_string(all_text)
            job['phone'] = self.extract_australia_phone_from_string(all_text)
            job['detailText'] = all_text.strip()
            lis = soup.xpath('//li/text()')
            job['requirements'] = lis
            self.detailed_jobs.put(job)
        else:
            raise Exception('Can not locate context')

    def _get_detail_job_by_selenium(self, job):
        browser = self.selenium_get(job['url'])
        try:
            elem = WebDriverWait(browser, 20).until(
                EC.presence_of_element_located((By.CLASS_NAME, "templatetext"))
            )
            side = browser.find_element_by_xpath('//aside').get_attribute("innerHTML")
            side = bs(side, "html.parser")
            job['salary'] = self.extract_salary_from_string(side.get_text())
            content_html = elem.get_attribute("innerHTML")
            soup = bs(content_html, "html.parser")
            all_text = soup.get_text()
            job['email'] = self.extract_email_from_string(all_text)
            job['phone'] = self.extract_australia_phone_from_string(all_text)
            job['detailText'] = all_text

            key_conditions = []

            lis = browser.find_elements_by_xpath('//aside')

            for l in lis:
                key_conditions.append(l.text)

            job['requirements'] = key_conditions
            self.detailed_jobs.put(job)
            browser.close()
        except Exception as e:
            print(e)
            raise Exception('Failed')

    def _get_detail_job(self, job):
        try:
            self._get_detail_job_by_request(job)
        except Exception as e:
            print(e)
            try:
                self._get_detail_job_by_selenium(job)
            except Exception as e:
                print(e)
                pass
