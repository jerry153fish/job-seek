from selenium import webdriver
phantomjs_path = '/usr/bin/phantomjs'
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.keys import Keys
from time import sleep
from random import randint, choice
from selenium.webdriver.support import expected_conditions as EC
from scrapies.user_agent import UserAgent
import requests, re

class ScrapyBase(object):
    def __init__(self, url):
        self.dcap = dict(DesiredCapabilities.PHANTOMJS)
        self.url = url

    def _new_agent_header(self):
        self.dcap["phantomjs.page.settings.userAgent"] = UserAgent.random_agent()
        return webdriver.PhantomJS(desired_capabilities=self.dcap)

    # selenium get
    def selenium_get(self, url):
        browser = self._new_agent_header()
        browser.get(url)
        return browser
    # request get
    def request_get(self, url):
        headers = requests.utils.default_headers()
        headers.update(
            {
                'User-Agent': UserAgent.random_agent(),
            }
        )
        return requests.get(url, headers=headers)

    def screen_shot(self, browser):
        sleep(2)
        browser.get_screenshot_as_file('screen.png')

    def extract_salary_from_string(self, str):
        sa = re.compile('[\$|\d|,]+\s+-\s+[\d|,|\$]+')
        m = re.search(sa, str)
        if m:
            return m.group(0).strip()
        else:
            return None


    def extract_email_from_string(self, str):
        sa = re.compile('[\w\.-]+@[\w\.-]+')
        m = re.search(sa, str)
        if m:
            return m.group(0).strip()
        else:
            return None

    def extract_australia_phone_from_string(self, str):
        sa = re.compile('[\d|\ |\(|\)|-]{8,}')
        m = re.search(sa, str)
        if m:
            return m.group(0).strip()
        else:
            return None
