# ########################## ###################### ############################
#                                                                              #
# Author: Jerry Chen <jerry153fish@gmail.com>                                  #
# Date: July 2017                                                              #
# License: MIT                                                                 #
#                                                                              #
# Model classes in jobseeker                                                   #
#                                                                              #
#                                                                              #
# ##############################################################################
from model import con, Base
from sqlalchemy import Table, Column, Integer, FLOAT, JSON, String, TEXT, ForeignKey, Enum, Date
from sqlalchemy.orm import relationship, backref

# 3rd table for Job and keywords
class JobKeyword(Base):
    __tablename__ = 'job_key'
    keyword_id = Column(Integer, ForeignKey('keywords.id'),primary_key=True)
    job_id = Column(Integer, ForeignKey('jobs.id'), primary_key=True)
    weight = Column(Integer, index=True)
    job = relationship('Job', backref=backref("job_keywords", cascade="all, delete-orphan" ))
    keyword = relationship('Keyword', backref=backref("job_keywords", cascade="all, delete-orphan" ))

# 3rd table for Experience and keywords
class ExperienceKeyword(Base):
    __tablename__ = 'exp_key'
    keyword_id = Column(Integer, ForeignKey('keywords.id'),primary_key=True)
    exp_id = Column(Integer, ForeignKey('experiences.id'), primary_key=True)
    weight = Column(Integer, index=True)
    experience = relationship('Experience', backref=backref("exp_keywords", cascade="all, delete-orphan" ))
    keyword = relationship('Keyword', backref=backref("exp_keywords", cascade="all, delete-orphan" ))

# 3rd table for Project and keywords
class ProjectKeyword(Base):
    __tablename__ = 'prj_key'
    keyword_id = Column(Integer, ForeignKey('keywords.id'),primary_key=True)
    prj_id = Column(Integer, ForeignKey('projects.id'), primary_key=True)
    weight = Column(Integer, index=True)
    project = relationship('Project', backref=backref("prj_keywords", cascade="all, delete-orphan" ))
    keyword = relationship('Keyword', backref=backref("prj_keywords", cascade="all, delete-orphan" ))

job_types =  Table(
    'job_type', # table name
    Base.metadata, # MetaData
    Column('jtype_id', ForeignKey('jtypes.id'), primary_key=True),
    Column('job_id', ForeignKey('jobs.id'), primary_key=True)
)

class UserJob(Base):
    __tablename__ = 'user_job'
    user_id = Column(Integer, ForeignKey('users.id'),primary_key=True)
    job_id = Column(Integer, ForeignKey('jobs.id'), primary_key=True)
    send = Column(Integer, index=True)
    user = relationship('User', backref=backref("user_job", cascade="all, delete-orphan" ))
    job = relationship('Job', backref=backref("user_job", cascade="all, delete-orphan" ))

class Job(Base):
    __tablename__ = 'jobs'
    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False)
    org_id = Column(Integer, ForeignKey('organizations.id'))
    type_id = Column(Integer, ForeignKey('jtypes.id'))
    locality_id = Column(Integer, ForeignKey('localities.id'))
    salary_id = Column(Integer, ForeignKey('salaries.id'))
    url = Column(String, nullable=False, unique=True)
    requirements = Column(TEXT)
    detailText = Column(TEXT)
    email = Column(String, index=True)
    phone= Column(String)
    employmentType = Column(String, index=True)
    description = Column(TEXT)
    datePosted = Column(Date, index=True)
    keywords = relationship('Keyword',
                            secondary='job_key', viewonly=True, lazy='joined')
    salary = relationship('Salary')
    organization = relationship('Organization', back_populates="jobs")
    jtype = relationship('Jtype',
                          secondary=job_types, back_populates="jobs")
    locality = relationship('Locality', back_populates="jobs")

    def add_keywords(self, kws):
        for keyword, weight in kws:
            self.job_keywords.append(JobKeyword(job=self, keyword=keyword, weight=weight))

class Jtype(Base):
    __tablename__ = 'jtypes'
    id = Column(Integer, primary_key=True)
    name = Column(String, unique=True)
    industry_id = Column(Integer, ForeignKey('industries.id'))
    jobs = relationship('Job', secondary=job_types, back_populates="jtype")
    industry = relationship('Industry', back_populates="jtype")

class Industry(Base):
    __tablename__ = 'industries'
    id = Column(Integer, primary_key=True)
    name = Column(String, unique=True)
    jtype = relationship('Jtype', back_populates="industry")

class Salary(Base):
    __tablename__ = 'salaries'
    id = Column(Integer, primary_key=True)
    start = Column(FLOAT, index=True)
    to = Column(FLOAT, index=True)
    currency = Column(String, index=True)

class Organization(Base):
    __tablename__ = 'organizations'
    id = Column(Integer, primary_key=True)
    name = Column(String, unique=True)
    description = Column(TEXT)
    rate = Column(FLOAT, index=True)
    jobs = relationship('Job', back_populates="organization")

class Keyword(Base):
    __tablename__ = 'keywords'
    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False, unique=True)

class Locality(Base):
    __tablename__ = 'localities'
    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False, unique=True)
    lat = Column(FLOAT)
    lng = Column(FLOAT)
    ghs = Column(String, index=True)
    jobs = relationship('Job', back_populates="locality")

class User(Base):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True)
    email = Column(String, nullable=False, unique=True)
    password = Column(String, nullable=False)
    profile_id = Column(Integer, ForeignKey('profiles.id'))
    profile = relationship('Profile')
    jobs = relationship('Job', secondary='user_job',
                          viewonly=True)

class Profile(Base):
    __tablename__ = 'profiles'
    id = Column(Integer, primary_key=True)
    first_name = Column(String)
    last_name = Column(String)
    gender = Column(Enum('male', 'female', 'other', name='gender_enum'))
    phone = Column(String)

class Experience(Base):
    __tablename__ = 'experiences'
    id = Column(Integer, primary_key=True)
    start = Column(Date)
    end = Column(Date)
    org_id = Column(Integer, ForeignKey('organizations.id'))
    posistion = Column(String)
    organization = relationship('Organization')
    description = Column(TEXT)
    achivements = Column(TEXT)
    keywords = relationship('Keyword', secondary='exp_key',
                          viewonly=True)

    def add_keywords(self, kws):
        for keyword, weight in kws:
            self.job_keywords.append(JobKeyword(job=self, keyword=keyword, weight=weight))

class Project(Base):
    __tablename__ = 'projects'
    id = Column(Integer, primary_key=True)
    start = Column(Date)
    end = Column(Date)
    responsibilities = Column(TEXT)
    description = Column(TEXT)
    achivements = Column(TEXT)

    keywords = relationship('Keyword', secondary='prj_key',
                          viewonly=True)

    def add_keywords(self, kws):
        for keyword, weight in kws:
            self.job_keywords.append(JobKeyword(job=self, keyword=keyword, weight=weight))

Base.metadata.drop_all(con)
Base.metadata.create_all(con)
