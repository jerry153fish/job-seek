import unittest
from model.model import Jtype
from model import con
from sqlalchemy.orm import sessionmaker
Session = sessionmaker(bind=con)
class TestMethods(unittest.TestCase):
    def setUp(self):
        self.session = Session()
    def tearDown(self):
        self.session.close()
    def test_upper(self):
        k = Jtype(name='bb')
        self.session.add(k)
        self.session.commit()
        t = self.session.query(Jtype).filter_by(name=k.name).first()
        self.assertEqual(t.name, 'bb')
        self.session.query(Jtype).filter_by(name=k.name).delete()
        self.session.commit()
