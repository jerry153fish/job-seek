import unittest
from model.model import Salary
from model import con
from sqlalchemy.orm import sessionmaker
Session = sessionmaker(bind=con)
class TestMethods(unittest.TestCase):
    def setUp(self):
        self.session = Session()
    def tearDown(self):
        self.session.close()
    def test_upper(self):
        k = Salary(start=7000, to=11000, currency="$")
        self.session.add(k)
        self.session.commit()
        t = self.session.query(Salary).filter_by(start=k.start, to=k.to).first()

        self.assertEqual(t.currency, '$')

        self.session.query(Salary).filter_by(id=t.id).delete()
        self.session.commit()
