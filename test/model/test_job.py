import unittest
from model.model import Job, Keyword
from model import con
from sqlalchemy.orm import sessionmaker
Session = sessionmaker(bind=con)

class TestMethods(unittest.TestCase):
    def setUp(self):
        self.session = Session()
    def tearDown(self):
        self.session.close()
    def test_job(self):
        k1 = Keyword(name='cc')
        k2 = Keyword(name='dd')
        self.session.add_all([k1, k2])
        self.session.commit()

        k = Job(name='bb', url='bb')
        k.add_keywords([(k1, 2), (k2, 4)])

        self.session.add(k)
        self.session.commit()
        t = self.session.query(Job).filter_by(name=k.url).first()

        self.assertEqual(t.name, 'bb')

        self.session.query(Job).filter_by(id=t.id).delete()
        self.session.query(Keyword).filter_by(id=k1.id).delete()
        self.session.query(Keyword).filter_by(id=k2.id).delete()
        self.session.commit()
