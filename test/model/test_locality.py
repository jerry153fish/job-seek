import unittest
from model.model import Locality
from utility import geohash
from model import con
from sqlalchemy.orm import sessionmaker
Session = sessionmaker(bind=con)
class TestMethods(unittest.TestCase):
    def setUp(self):
        self.session = Session()
    def tearDown(self):
        self.session.close()
    def test_upper(self):
        ghs = geohash.encode(140.001, -147.331, precision=9)
        k = Locality(name='Hobart', lat=140.001, lng=-147.331, ghs=ghs)
        self.session.add(k)
        self.session.commit()
        t = self.session.query(Locality).filter_by(ghs=k.ghs).first()
        self.assertEqual(t.name, 'Hobart')
        self.session.query(Locality).filter_by(id=t.id).delete()
        self.session.commit()
