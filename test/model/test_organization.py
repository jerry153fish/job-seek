import unittest
from model.model import Organization
from model import con
from sqlalchemy.orm import sessionmaker
Session = sessionmaker(bind=con)
class TestMethods(unittest.TestCase):
    def setUp(self):
        self.session = Session()
    def tearDown(self):
        self.session.close()
    def test_upper(self):
        k = Organization(name='bb', description='aa', rate=3.5)
        self.session.add(k)
        self.session.commit()
        t = self.session.query(Organization).filter_by(name=k.name).first()

        self.assertEqual(t.name, 'bb')
        self.assertEqual(t.rate, 3.5)
        self.session.query(Organization).filter_by(id=t.id).delete()
        self.session.commit()
