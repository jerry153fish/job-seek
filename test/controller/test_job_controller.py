import unittest
from controller.jobcontroller import JobController
from model.model import Keyword

class TestMethods(unittest.TestCase):
    def setUp(self):
        self.jc = JobController({'location': 'Hobart', 'datePosted': '2017-05-30T23:35:51Z', 'employmentType': 'Full Time', 'jtype': {'name': 'Developers/Programmers', 'industry': 'Information & Communication Technology'}, \
         'email': 'john.knight@saltworth.com.au', 'detailText': "Automation Build / Infrastructure Programmer", 'name': 'Automation Build / Infrastructure Programmer - DevOps Engineer - Docker / Puppet', \
         'url': 'https://www.seek.com.au/job/33531761', 'organization': {'@type': 'Organization', 'name': 'Saltworth® Pty Ltd'}, '@context': 'http://schema.org', 'phone': '', 'requirements': ['Deliver features and functionality to create a best-in-market platform solution\n', 'Promote continuous improvement, aligns and works with peers to ensure progressive solutions and methods are implemented, as appropriate\n', 'Participate and contribute to team DEVOPS culture and practices ', 'Promote and guide the use of infrastructure automation within IT ', 'Extensive use of automation to reduce risk and eliminate time-consuming repetitive manual tasks\n', 'Has consideration for architecture and application design aspects during delivery\n', 'Works with technical leads and architecture leads to create technically superior platform features\n', 'Deliver simple and robust solutions using a variety of technical means.', 'Demonstrable Coding and Scripting experience ', 'Ruby or other dynamic scripting languages ', 'Puppet, Ansible or equivalent automation languages ', 'Operating System knowledge Linux/UNIX and Windows ', 'MYSQL ', 'Docker ', 'Knowledge of Network and Internet technologies ',   'Microservices and distributed architecture', 'Java, Groovy, Javascript, Delphi (legacy)', 'Dropwizard, Spring Boot', 'Gradle and Maven', 'MongoDB and MySQL', 'Kafka and RabbitMQ', 'Hystrix', 'Docker', 'JUnit, Spock, Mockito'], \
         'salary': '$100,000 - $139,999', '@type': 'JobPosting', 'description': 'Infrastructure Programmer / Dev Ops build & automation engineer with good coding skills, a love of automation and all things Docker, Puppet & similar'})
    def tearDown(self):
        pass
    def test_save(self):
        result = self.jc.save()
        self.assertTrue(result)
        self.jc.delete()
