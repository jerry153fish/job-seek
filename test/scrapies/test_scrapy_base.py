import unittest
from scrapies.scrapy_base import ScrapyBase

class TestMethods(unittest.TestCase):
    def setUp(self):
        self.sb = ScrapyBase('https://google.com')
        self.testString = 'asdf 12@.com.au asdt 0535826586 $7,000 - $8,000'
    def tearDown(self):
        pass
    def test_email(self):
        email = self.sb.extract_email_from_string(self.testString)
        self.assertEqual(email, '12@.com.au')
    def test_phone(self):
        phone = self.sb.extract_australia_phone_from_string(self.testString).strip()
        self.assertEqual(phone, '0535826586')
    def test_salary(self):
        salary = self.sb.extract_salary_from_string(self.testString).strip()
        self.assertEqual(salary, '$7,000 - $8,000')
