# ########################## ###################### ############################
#                                                                              #
# Author: Jerry Chen <jerry153fish@gmail.com>                                  #
# Date: July 2017                                                              #
# License: MIT                                                                 #
#                                                                              #
# job controller for process raw data from scrapy                              #
#                                                                              #
#                                                                              #
# ##############################################################################
from model.model import Job, Jtype, Industry, Salary, Organization, Keyword, Locality, User, Profile, UserJob
from model import con, Session, session_scope
import nltk, re
import json
from dateutil.parser import parse

from controller import generate_job_keywords

class UserController(object):
    pass
