# ########################## ###################### ############################
#                                                                              #
# Author: Jerry Chen <jerry153fish@gmail.com>                                  #
# Date: July 2017                                                              #
# License: MIT                                                                 #
#                                                                              #
# Ntlk simple use for keywords handling                                        #
#                                                                              #
#                                                                              #
# ##############################################################################
from nltk.corpus import stopwords
import nltk, re

sps = stopwords.words('english')

def generate_job_keywords(text):
    raw_keys = [i.lower() for i in nltk.word_tokenize(text) if i not in sps]
    taged_keys = nltk.pos_tag(raw_keys)
    return list(set([k[0] for k in taged_keys if k[1] == 'NN']))
