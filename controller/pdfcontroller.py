# ########################## ###################### ############################
#                                                                              #
# Author: Jerry Chen <jerry153fish@gmail.com>                                  #
# Date: July 2017                                                              #
# License: MIT                                                                 #
#                                                                              #
# generate pdf resume interms of profile                                      #
#                                                                              #
#                                                                              #
# ##############################################################################

import pdfkit
from jinja2 import Environment, PackageLoader, select_autoescape
import os
env = Environment(
    loader=PackageLoader('templates', 'cv'),
    autoescape=select_autoescape(['html', 'xml'])
)

class PdfController(object):
    def __init__(self, template='resume.html', css='templates/cv/style.css'):
        self.template = env.get_template(template)
        self.css = css
        self.options = {
            'page-size': 'Letter',
            'lowquality': False
        }

    def generate_pdf(self, profile, out=False):
        return pdfkit.from_string(self.template.render(profile), out , options=self.options, css=self.css)

# tt = env.get_template('resume.html')
# profile = {'name': 'jerry', 'statement': 'GOod'}
#
# if not os.path.exists(out):
#     os.makedirs(out)
# ops = {
#     'page-size': 'Letter',
#     'lowquality': False
# }
#
# pdfkit.from_string(tt.render(profile), 'test.pdf' , options=ops, css=css)
