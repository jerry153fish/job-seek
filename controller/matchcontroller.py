# ########################## ###################### ############################
#                                                                              #
# Author: Jerry Chen <jerry153fish@gmail.com>                                  #
# Date: July 2017                                                              #
# License: MIT                                                                 #
#                                                                              #
# Generate profile resume interms of job details and user details              #
#                                                                              #
#                                                                              #
# ##############################################################################
from controller import generate_job_keywords

class MatchController(object):
    def __init__(self, job, user):
        self.job = job
        self.user = user

    def generate_profile(self):
        pass

    def keyword_score(self, job_keys, item):
        score = 0
        for key in item['keywords']:
            if key[0] in job_keys:
                score += key[1]
        return score, item

    def most_match_items(self, job_keys, items, num):
        if(len(items) > num):
            items_score = [self.keyword_score(job_keys, item) for item in items]
            items_score = sorted(items_score, key=lambda tup: tup[0], reverse=True)
            return [item for score, item in items_score[:num]]
        else:
            return items
