# ########################## ###################### ############################
#                                                                              #
# Author: Jerry Chen <jerry153fish@gmail.com>                                  #
# Date: July 2017                                                              #
# License: MIT                                                                 #
#                                                                              #
# job controller for process raw data from scrapy                              #
#                                                                              #
#                                                                              #
# ##############################################################################
from model.model import Job, Jtype, Industry, Salary, Organization, Keyword, Locality, User, Profile, UserJob
from model import con, Session, session_scope
import nltk, re
import json
from dateutil.parser import parse

from controller import generate_job_keywords


class JobController(object):
    def __init__(self, job):
        self.job = job
        self.instance = None
    # save job in database
    def save(self):
        with session_scope() as session:
            job = session.query(Job).filter_by(url=self.job['url']).first()
            self.instance = job
            if not job:
                datePosted = parse(self.job['datePosted'])
                job = Job(
                    name=self.job['name'],
                    datePosted=datePosted,
                    url=self.job['url'],
                    email=self.job['email'],
                    phone=self.job['phone'],
                    description=self.job['description'],
                    detailText=self.job['detailText'],
                    requirements=json.dumps(self.job['requirements']),
                    employmentType=self.job['employmentType']
                )
                jtype = self._create_or_get_jtype(session, self.job['jtype'])
                if 'salary' in self.job and self.job['salary'] is not None:
                    salary = self._create_or_get_salary(session, self.job['salary'])
                    job.salary = salary
                org = self._get_or_create(session, Organization, name=self.job['organization']['name'])
                loc = self._get_or_create(session, Locality, name=self.job['location'])
                kws = [(self._get_or_create(session, Keyword, name=k), 1) for k in generate_job_keywords(self.job['detailText'])]
                job.jtype = [jtype]

                job.organization = org
                job.location = loc
                job.add_keywords(kws)
                session.add(job)
                session.commit()
                self.instance = job
                return True
            else:
                print('already existed')
                return False

    # notice user
    def notice(self):
        pass
    # delete job from database
    def delete(self):
        with session_scope() as session:
            try:
                session.delete(self.instance)
            except Exception as e:
                raise

    def isSaved(session, url):
        job = session.query(Job).filter_by(url=url).first()
        if job:
            return True
        else:
            return False

    def isSend(session, url, uerEmail):
        userJob = session.qer

    # create job type
    def _create_or_get_jtype(self, session, jtype):
        instance = session.query(Jtype).filter_by(name=jtype['name']).first()
        if instance:
            return instance
        else:
            industry = self._create_or_get_industry(session, jtype['industry'])
            jp = Jtype(name=jtype['name'])
            jp.industry = industry
            return jp
    # create industray catetory
    def _create_or_get_industry(self, session, industry):
        with session_scope() as session:
            instance = session.query(Industry).filter_by(name=industry).first()
            if instance:
                return instance
            else:
                return Industry(name=industry)
    # get or create intanse of given model in database
    def _get_or_create(self,session, model, **kwargs):
        instance = session.query(model).filter_by(**kwargs).first()
        if instance:
            return instance
        else:
            return model(**kwargs)

    def _create_or_get_salary(self, session, salary):
        salary_pattern = re.compile('[\d+((.|,)\d+)]+')
        salaries = list(re.findall(salary_pattern, salary))
        if len(salaries) > 1:
            start = float(re.sub('[.|,]', '', salaries[0]))
            to = float(re.sub('[.|,]', '',salaries[1]))
        elif len(salaries) == 1:
            start = float(re.sub('[.|,]', '', salaries[0]))
            to = float(re.sub('[.|,]', '', salaries[0]))
        currency = salary.strip()
        currency = currency[0]
        instance = session.query(Salary).filter_by(start=start, to=to, currency=currency).first()
        if instance:
            return instance
        else:
            return Salary(start=start, to=to, currency=currency)
