from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication
import smtplib

def send_email(user, pwd, recipient, subject, body, pdf_source):
    msg = MIMEMultipart()
    gmail_user = user
    gmail_pwd = pwd
    FROM = user
    TO = recipient if type(recipient) is list else [recipient]
    SUBJECT = subject
    TEXT = body
    msg['Subject'] = subject
    msg['From'] = user

    # Prepare actual message
    message = """From: %s\nTo: %s\nSubject: %s\n\n%s
    """ % (FROM, ", ".join(TO), SUBJECT, TEXT)
    part1 = MIMEText(message)
    msg.attach(part1)

    # Prepare pdf

    pdf = MIMEApplication(pdf_source, _subtype = 'pdf')
    pdf.add_header('content-disposition', 'attachment', filename='cv')
    msg.attach(pdf)


    try:
        server = smtplib.SMTP("smtp.gmail.com", 587)
        server.ehlo()
        server.starttls()
        server.login(gmail_user, gmail_pwd)
        server.sendmail(FROM, TO, msg.as_string())
        server.close()
        print('successfully sent the mail')
    except:
        print("failed to send mail")
