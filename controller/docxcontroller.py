# ########################## ###################### ############################
#                                                                              #
# Author: Jerry Chen <jerry153fish@gmail.com>                                  #
# Date: July 2017                                                              #
# License: MIT                                                                 #
#                                                                              #
# generate docx resume interms of profile                                      #
#                                                                              #
#                                                                              #
# ##############################################################################
from docxtpl import DocxTemplate

class DocxController(object):
    def __init__(self, template='templates/cv/resume.docx'):
        self.tem = DocxTemplate(template)
    def generate_docx(self, profile, out):
        self.tem.render(profile)
        self.tem.save(out)
